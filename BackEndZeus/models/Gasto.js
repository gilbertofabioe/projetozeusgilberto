const mongoose = require('mongoose')

const Gasto = mongoose.model('Gasto', {
    data: String,
    valor: Number,
    comida: Boolean,
    vet: Boolean,
    banho: Boolean,
})

module.exports = Gasto