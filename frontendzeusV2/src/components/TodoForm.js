
import '../App.css';
import React, {useState, useEffect} from 'react'
import {BsTrash, BsBookmarkCheck, BsBookmarkCheckFill} from 'react-icons/bs'
import {FiEdit3} from 'react-icons/fi' 
import TodoList from './TodoList';

//const API = "http://localhost:5000";
const API = "http://localhost:5001";
const DataBase = "/gastos";


const TodoForm = (props) => {

    const [title, setTitle] = useState('');
    const [valor, setValor] = useState();
    const [banho, setBanho] = useState(false);
    const [vet, setVet] = useState(false);
    const [comida, setComida] = useState(false);
    const [todos, setTodos] = useState([]);
    const [loading, setLoaing] = useState(false);
    const [editing, setEditing] = useState(null);
    const [deleting, setDeleting] = useState(null);
    const [editValue, setEditValue] = useState({});
    const [month, setMonth] = useState(0);
    const [submit, setSubmit] = useState(false);
    const [data, setData] = useState(new Date().toISOString().split('T')[0])
    //Carregar gastos no pageload
    useEffect(()=>{
      const loadData = async() =>{
        //setLoaing(true);
        const res = await fetch(API + "/gastos")
        .then((res)=> res.json())
        .then((data)=>data)
        .catch((err)=>console.log(err))
        //setLoaing(false);
        setTodos(res);
        setSubmit(false)  
      };
      loadData();
    },[props.refresh])

  
    const currentDate = () =>{
      const data =  new Date().toISOString().split('T')[0]
      return data
    }

  
    const handleSubmit = async (e)=>{
      e.preventDefault(); 
      const valorFormat = parseFloat(valor).toFixed(2)
      const date = data;
      const todo = {
        date,
        title,
        valorFormat,
        banho,
        vet,
        comida,
      };
      
      //enviar para DB
      console.log(todo)
      await fetch(API + DataBase,{
        method: "POST",
        body: JSON.stringify(todo),
        headers:{
          "Content-Type": "application/json",
        },
      }).then(alert(`${todo.title} foi adicionado com sucesso!`)).then(setSubmit(true));
      props.setRefresh(!props.refresh)
      setTitle('')
      setValor()
      setData(new Date().toISOString().split('T')[0])
      setBanho(false)
      setVet(false)
      setComida(false)
      props.setModalForm(false)
          
    };
  
    if(loading){
      return <p>Carregando...</p>
    }

    const handleForm = () =>{
      props.setModalForm(false)
    }
  
    return (
      
      <div className="todo-form">
        <div className='form-todo'>
          <h2>Insira sua próxima compra:</h2>
          <div className='form'>
            <form onSubmit={handleSubmit}>
              <div className='form-control'>
                <label htmlFor='title'>Qual o seu gasto?</label>
                <input 
                  type="text" 
                  name="title" 
                  placeholder="Informe o gasto" 
                  onChange={(e) => setTitle(e.target.value)}
                  value={title || ""}
                  required 
                />
              </div>
              <div className='form-control'>
                <label htmlFor='valor'>Qual o valor do seu gasto?</label>
                <input 
                  type="number" 
                  name="valor" 
                  placeholder="Informe o valor do gasto" 
                  onChange={(e) => setValor(e.target.value)}
                  value={valor || ""}
                  required 
                />
              </div>
              <div className='form-control'>
                <label htmlFor='data'>Qual foi a data do gasto?</label>
                <input 
                  type="date" 
                  name="data"  
                  defaultValue={currentDate()}
                  onChange={(e) => setData(e.target.value)}
                  required 
                />
              </div>
              <div className='actions'>
                <span title='Banho'>Banho: 
                  {!banho ? <BsBookmarkCheck onClick={()=>setBanho(!banho)}/>:<BsBookmarkCheckFill onClick={()=>setBanho(!banho)}/>}
                </span>
                <span title='Vet'> Vet: 
                  {!vet ? <BsBookmarkCheck onClick={()=>setVet(!vet)}/>:<BsBookmarkCheckFill onClick={()=>setVet(!vet)}/>}
                </span>
                <span title='Comida'> Comida: 
                  {!comida ? <BsBookmarkCheck onClick={()=>setComida(!comida)}/>:<BsBookmarkCheckFill onClick={()=>setComida(!comida)}/>}
                </span> 
              </div>
              <input type="submit" value="Adicionar Gasto" />
            </form>
            <button onClick={handleForm}>Cancelar</button>
          </div>
        </div>
      </div>
    );
  }

export default TodoForm