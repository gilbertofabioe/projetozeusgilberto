import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, TextInput, Switch, Button, Alert, AppRegistry } from 'react-native';
import React, {Component} from 'react';
import Zeus from './components/Zeus';
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';

const API = "http://localhost:5001";
const DataBase = "/gastos";

const client = new ApolloClient({
  uri: 'localhost:5001/gastos',
  cache: new InMemoryCache(),
  credentials: 'include'
})


const App = () =>{
  <ApolloProvider client={client}>
    <MyRootComponent />
  </ApolloProvider>
  return (
    <Zeus/>
  )
}
AppRegistry.registerComponent('MyApplication', ()=>App);
export default App