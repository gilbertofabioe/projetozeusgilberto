import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, TextInput, Switch, Button, Alert, AppRegistry } from 'react-native';
import React, {Component} from 'react';
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';



const Zeus = () =>{

  

    //const [loading, setLoaing] = useState(false);
    //const [editing, setEditing] = useState(null);
    //const [editValue, setEditValue] = useState({});


    const [text, onChangeText] = React.useState('');
    const [title, setTitle] = React.useState('');
    const [valor, setValor] = React.useState();
    const [banho, setBanho] = React.useState(false);
    const [vet, setVet] = React.useState(false);
    const [comida, setComida] = React.useState(false);
    const [todos, setTodos] = React.useState([]);
    const toggleVet = () => setVet(previousState => !previousState);
    const toggleComida = () => setComida(previousState => !previousState);
    const toggleBanho = () => setBanho(previousState => !previousState);



      const handleSubmit = () =>{
        const valorFormat = parseFloat(valor).toFixed(2)
        const date = new Date().toLocaleDateString();
  
      const todo = {
        date,
        title,
        valorFormat,
        banho,
        vet,
        comida,
      };

      setTitle('')
      setValor()


      //setTodos((prevState)=>[...prevState, todo]);

        Alert.alert(`O valor de Titulo é: ${title}, e o valor é: ${valorFormat}, vet é ${vet}`)
    }


    return(
    
      <View style={styles.container}>
        <Text style={styles.Text}>My Zeus Project</Text>
        <TextInput 
            style={styles.Input} 
            placeholder="Informar seu gasto"
            onChangeText={setTitle}
            value={title}
            />
        <TextInput
            style={styles.Input} 
            keyboardType="numeric" 
            placeholder="Informar o valor"
            onChangeText={setValor}
            value={valor}
           />
        <View style={styles.viewBox}> 
          <Text style={styles.toggleText}>Banho:</Text>
          <Switch
          style={styles.toggle}
              trackColor={{false: "#767577", true:"#81b0ff"
              }}
              thumbColor={banho? "#f5dd4b" : "#f4f3f4"}
              ios_backgroundColor="#3e3e3e"
              onValueChange={toggleBanho}
              value={banho}
              
          />
        </View>
        <View style={styles.viewBox}> 
        <Text style={styles.toggleText}>Veterinário:</Text>
          <Switch
          style={styles.toggle}
              trackColor={{false: "#767577", true:"#81b0ff"
              }}
              thumbColor={vet? "#f5dd4b" : "#f4f3f4"}
              ios_backgroundColor="#3e3e3e"
              onValueChange={toggleVet}
              value={vet}
              
          />
        </View>
        <View style={styles.viewBox}> 
          <Text style={styles.toggleText}>Comida:</Text>
          <Switch
          style={styles.toggle}
              trackColor={{false: "#767577", true:"#81b0ff"
              }}
              thumbColor={comida? "#f5dd4b" : "#f4f3f4"}
              ios_backgroundColor="#3e3e3e"
              onValueChange={toggleComida}
              value={comida}
              
          />
        </View>
        
        <Button
        title="Press me"
        onPress={handleSubmit}
        />    

        <StatusBar style='auto'/>

      </View>
    )
  }


const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 8,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  Text: {
    fontSize: 25,
    color: '#4a84b5',
    fontWeight: 'bold',
    backgroundColor: '#e7f8fd',
    borderRadius: 5,
    padding: 10,
    margin: 55,
    borderWidth: 1,
    borderColor: '#f3fcfe',
    width: '90%',
    textAlign: 'center'
  },
  Input: {
    fontSize: 30,
    color: '#689297',
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    borderColor: '#79867f',
    margin: 15,
    padding: 10,
    width: '90%'
  },

  viewBox: {
    backgroundColor: '#f3fcfe',
    width: '90%',
    margin: 2,
    padding: 5,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignSelf: 'center'
  },
  toggle: {
    direction: 'ltr'
  },
  toggleText: {
    direction: 'rtl'
  }
});


export default Zeus