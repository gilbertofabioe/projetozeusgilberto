import AsyncStorage from '@react-native-async-storage/async-storage';
const API = 'http://172.18.9.229:5001/gastos'
async function saveItem(listItem, id) {
    listItem.id = id ? id : new Date().getTime()
    const savedItems = await getItems();

    if (id) {
        const index = await savedItems.findIndex(item => item.id === id);
        savedItems[index] = listItem;
    }
    else
        savedItems.push(listItem);

    return AsyncStorage.setItem('items', JSON.stringify(savedItems));
}

async function getItems() {
    const res = await fetch(API).then((res)=>res.json()).catch((err)=> console.log(err))
    //console.log(`O objeto do DB é: ${Object.keys(res[0])}`)
    return Promise.resolve(res)
}

async function getItem(id) {
    const savedItems = await getItems();
    return savedItems.find(item => item.id === id);
}

async function deleteItem(id) {
    let savedItems = await getItems();
    const index = await savedItems.findIndex(item => item.id === id);
    savedItems.splice(index, 1);
    return AsyncStorage.setItem('items', JSON.stringify(savedItems));
}

module.exports = {
    saveItem,
    getItems,
    getItem,
    deleteItem
}