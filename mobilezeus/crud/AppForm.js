import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity,Switch, Alert } from 'react-native';
import { Feather as Icon } from '@expo/vector-icons';
import Database from './Database';


export default function AppForm({ route, navigation }) {
    //const _id = route.params ? route.params._id : undefined;
    const [_id, setId] = React.useState(undefined)
    console.log(_id)
    const [title, setTitle] = React.useState('');
    const [valorFormat, setValorFormat] = React.useState();
    const [banho, setBanho] = React.useState(false);
    const [vet, setVet] = React.useState(false);
    const [comida, setComida] = React.useState(false);
    const toggleVet = () => setVet(previousState => !previousState);
    const toggleComida = () => setComida(previousState => !previousState);
    const toggleBanho = () => setBanho(previousState => !previousState);

    useEffect(() => {
        if (!route.params) return;
        setId(route.params._id)
        setTitle(route.params.title)
        setValorFormat(route.params.valorFormat.toString())
        setBanho(route.params.banho)
        setComida(route.params.comida)
        setVet(route.params.vet)
    }, [route])

    function handleTitleChange(title){ setTitle(title); }
    function handleValorChange(valorFormat){ 
      const formatado = parseFloat(valorFormat).toFixed(2)
      setValorFormat(valorFormat); }

    async function handleButtonPress() {
      if(title !== '' && valorFormat !== '' &&!isNaN(valorFormat)){
        const date = new Date().toLocaleDateString();
        const listItem = {
          date,
          title,
          valorFormat: parseFloat(valorFormat).toFixed(2),
          banho,
          vet,
          comida,
        };       
        //console.log(listItem)
        setTitle('')
        setValorFormat('')
        setBanho(false)
        setComida(false)
        setVet(false)

        if(_id){
          Database.editItem(listItem, _id)
          .then(response => navigation.navigate("AppList", listItem)).then(setId(undefined));
          return;
        }
        Database.saveItem(listItem)
        .then(response => navigation.navigate("AppList", listItem));
        
  
        /*Database.saveItem(listItem, _id)
            .then(response => navigation.navigate("AppList", listItem));*/
       
      }else{
        Alert.alert("Por favor preencher os campos de gasto e valor corretamente!")
      }

    }

    return (
        <View style={styles.container}>
            <Text style={styles.title}>Gasto:</Text>
            <View style={styles.inputContainer}>
                <TextInput
                    style={styles.input}
                    onChangeText={handleTitleChange}
                    placeholder="Informe o gasto."
                    clearButtonMode="always"
                    value={title} />
                <TextInput
                    style={styles.input}
                    onChangeText={handleValorChange}
                    placeholder="Informe o valor do gasto"
                    keyboardType={'numeric'}
                    clearButtonMode="always"
                    value={valorFormat} />
                <View style={styles.viewBox}>
         <Text style={styles.toggleText}>Banho:</Text>
         <Switch
         style={styles.toggle}
             trackColor={{false: "#767577", true:"#81b0ff"
             }}
             thumbColor={banho? "#f5dd4b" : "#f4f3f4"}
             ios_backgroundColor="#3e3e3e"
             onValueChange={toggleBanho}
             value={banho}   
           />
        </View>
        <View style={styles.viewBox}>
        <Text style={styles.toggleText}>Veterinário:</Text>
          <Switch
          style={styles.toggle}
              trackColor={{false: "#767577", true:"#81b0ff"
              }}
              thumbColor={vet? "#f5dd4b" : "#f4f3f4"}
              ios_backgroundColor="#3e3e3e"
              onValueChange={toggleVet}
              value={vet}
              
          />
        </View>
        <View style={styles.viewBox}>
          <Text style={styles.toggleText}>Comida:</Text>
          <Switch
          style={styles.toggle}
              trackColor={{false: "#767577", true:"#81b0ff"
              }}
              thumbColor={comida? "#f5dd4b" : "#f4f3f4"}
              ios_backgroundColor="#3e3e3e"
              onValueChange={toggleComida}
              value={comida}
              
          />
        </View>   
                <TouchableOpacity style={styles.button} onPress={handleButtonPress}>
                    <View style={styles.buttonContainer}>
                        <Icon name="save" size={22} color="white" />
                        <Text style={styles.buttonText}>Salvar</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <StatusBar style="light" />
        </View>
    );
}
 
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#30c9cf',
      alignItems: 'center',
    },
    title: {
      color: '#fff',
      fontSize: 20,
      fontWeight: 'bold',
      marginTop: 50,
    },
    inputContainer: {
      flex: 1,
      marginTop: 30,
      width: '90%',
      padding: 20,
      borderTopLeftRadius: 10,
      borderTopRightRadius: 10,
      alignItems: 'stretch',
      backgroundColor: '#fff',

    },
    input: {
      marginTop: 10,
      height: 60,
      backgroundColor: '#fff',
      borderRadius: 10,
      paddingHorizontal: 24,
      fontSize: 16,
      alignItems: 'stretch',
      borderColor: '#FEF4F2',
      borderBottomWidth: 1,
      borderLeftWidth: 1
    },
    button: {
      marginTop: 10,
      height: 60,
      backgroundColor: '#CF3630',
      borderRadius: 10,
      paddingHorizontal: 24,
      fontSize: 16,
      alignItems: 'center',
      justifyContent: 'center',
      elevation: 20,
      shadowOpacity: 20,
      shadowColor: '#ccc',
    },
    buttonText: {
      color: '#fff',
      fontWeight: 'bold',
      fontSize: 19
    },
    viewBox: {
        marginTop: 10,
        height: 55,
        backgroundColor: '#FEF4F2',
        borderRadius: 10,
        paddingHorizontal: 24,
        fontSize: 16,
        alignItems: 'stretch'
      },
      toggle: {
        direction: 'ltr'
      },
      toggleText: {
        direction: 'rtl'
      }
  });